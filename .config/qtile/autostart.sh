#!/bin/bash
eval $(gnome-keyring-daemon --start)
export SSH_AUTH_SOCK
picom &
lxsession &
twmnd &
nm-applet &
~/.fehbg &
emacs --daemon &
discord &
megasync &
steam-native -silent &
mpc add / && mpc shuffle &
