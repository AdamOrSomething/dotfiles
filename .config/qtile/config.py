import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, EzKey, Key, Match, Screen, Rule, ScratchPad, DropDown
from libqtile import layout, bar, widget, hook
from libqtile.command import lazy
from libqtile.dgroups import simple_key_binder
from typing import List

mod       = 'mod4'
myTerm    = 'alacritty'
myBrowser = 'surf'

colors = [
    ['#2E3440', '#2E3440'], # panel background
    ['#3d3f4b', '#434758'], # background for current screen tab
    ['#88C0D0', '#88C0D0'], # font color for group names
    ['#ff5555', '#ff5555'], # border line color for current tab
    ['#4C566A', '#4C566A'], # border line color for 'other tabs' and color for 'odd widgets'
    ['#434C5E', '#434C5E'], # color for the 'even widgets'
    ['#88C0D0', '#88C0D0'], # window name
    ['#D8DEE9', '#D8DEE9']  # background for inactive screens
]

# Nord colors
nord = [
    ['#2E3440', '#2E3440'],
    ['#3B4252', '#3B4252'],
    ['#434C5E', '#434C5E'],
    ['#4C566A', '#4C566A'],
    ['#D8DEE9', '#D8DEE9'],
    ['#E5E9F0', '#E5E9F0'],
    ['#ECEFF4', '#ECEFF4'],
    ['#8FBCBB', '#8FBCBB'],
    ['#88C0D0', '#88C0D0'],
    ['#81A1C1', '#81A1C1'],
    ['#5E81AC', '#5E81AC'],
    ['#BF616A', '#BF616A'],
    ['#D08770', '#D08770'],
    ['#EBCB8B', '#EBCB8B'],
    ['#A3BE8C', '#A3BE8C'],
    ['#BR8EAD', '#BR8EAD']
]

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

keys = [
    EzKey('M-<Return>',
        lazy.spawn('dmenu_run'),
        desc='Launch dmenu'
    ),
    EzKey('M-S-<Return>',
        lazy.spawn(myTerm),
        desc='Launch terminal'
    ),
    EzKey('M-b',
        lazy.spawn(myBrowser),
        desc='Launch browser'
    ),
    EzKey('M-v',
        lazy.spawn('emacsclient -c'),
        desc='Launch Doom Emacs'
    ),
    EzKey('<Print>',
        lazy.spawn('flameshot gui'),
        desc='Launch Flameshot'
    ),
    EzKey('M-<Escape>',
        lazy.spawn('.local/bin/lock'),
        desc='Lock screen'
    ),

    EzKey('<XF86AudioPlay>',
        lazy.spawn('mpc toggle'),
        desc='Play/pause current file in mpd'
    ),
    EzKey('<XF86AudioPrev>',
        lazy.spawn('mpc prev'),
        desc='Go to previous in mpd'
    ),
    EzKey('<XF86AudioNext>',
        lazy.spawn('mpc next'),
        desc='Go to next in mpd'
    ),

    EzKey('M-S-r',
        lazy.restart(),
        desc='Restart Qtile'
    ),
    EzKey('M-S-q',
        lazy.shutdown(),
        desc='Shutdown Qtile'
    ),

    EzKey('M-w',
        lazy.to_screen(0),
        desc='Focus monitor 1'
    ),
    EzKey('M-e',
        lazy.to_screen(1),
        desc='Focus monitor 2'
    ),
    EzKey('M-S-w',
        lazy.function(window_to_previous_screen),
        desc='Move focused window to previous monitor'
    ),
    EzKey('M-S-e',
        lazy.function(window_to_next_screen),
        desc='Move focused window to next monitor'
    ),

    EzKey('M-<grave>',
        lazy.screen.toggle_group(),
        desc='Move to the last used group'
    ),
    EzKey('M-<period>',
        lazy.screen.next_group(),
        desc='Move to the group on the right'
    ),
    EzKey('M-<comma>',
        lazy.screen.prev_group(),
        desc='Move to the group on the left'
    ),
    EzKey('M-<slash>',
        lazy.group['scratchpad'].dropdown_toggle('alacritty')
    ),

    EzKey('M-<space>',
        lazy.next_layout(),
        desc='Cycle to next layout'
    ),
    EzKey('M-S-<space>',
        lazy.prev_layout(),
        desc='Cycle to previous layout'
    ),
    EzKey('M-<Tab>',
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side master occupies (MonadTall)'
    ),

    EzKey('M-S-c',
        lazy.window.kill(),
        desc='Close active window'
    ),
    EzKey('M-f',
        lazy.window.toggle_fullscreen(),
        desc='Toggle fullscreen'
    ),
    EzKey('M-S-f',
        lazy.window.toggle_floating(),
        desc='Toggle floating'
    ),

    EzKey('M-j',
        lazy.layout.down(),
        desc='Move focus down in current stack'
    ),
    EzKey('M-k',
        lazy.layout.up(),
        desc='Move focus up in current stack'
    ),
    EzKey('M-S-j',
        lazy.layout.shuffle_down(),
        lazy.layout.section_down(),
        desc='Move windows down in current stack'
    ),
    EzKey('M-S-k',
        lazy.layout.shuffle_up(),
        lazy.layout.section_up(),
        desc='Move windows up in current stack'
    ),
    EzKey('M-h',
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease master pane (Tile)'
    ),
    EzKey('M-l',
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase master pane (Tile)'
    ),
    EzKey('M-n',
        lazy.layout.normalize(),
        desc='Normalize window sizes'
    ),
    EzKey('M-m',
        lazy.layout.maximize(),
        desc='Minimize/maximize windows'
    ),

    # Dmenu scripts launched using the key chord SUPER+p followed by 'key'
    KeyChord([mod], 'p', [
        Key([], 'e',
            lazy.spawn('./dmscripts/dm-confedit'),
            desc='Choose a config file to edit'
        ),
        Key([], 'i',
            lazy.spawn('./dmscripts/dm-maim'),
            desc='Take screenshots via dmenu'
        ),
        Key([], 'k',
            lazy.spawn('./dmscripts/dm-kill'),
            desc='Kill processes via dmenu'
        ),
        Key([], 'l',
            lazy.spawn('./dmscripts/dm-logout'),
            desc='A logout menu'
        ),
        Key([], 'm',
            lazy.spawn('./dmscripts/dm-man'),
            desc='Search manpages in dmenu'
        ),
        Key([], 'o',
            lazy.spawn('./dmscripts/dm-bookman'),
            desc='Search your qutebrowser bookmarks and quickmarks'
        ),
        Key([], 'r',
            lazy.spawn('./dmscripts/dm-reddit'),
            desc='Search reddit via dmenu'
        ),
        Key([], 's', lazy.spawn('./dmscripts/dm-websearch'),
            desc='Search various search engines via dmenu'
        ),
        Key([], 'p',
            lazy.spawn('passmenu'),
            desc='Retrieve passwords with dmenu'
        )
    ])
]

group_names = [
    {
        'name': 'dev',
        'layout': 'monadtall',
        'position': 1
    },
    {
        'name': 'ref',
        'layout': 'monadtall',
        'position': 2
    },
    {
        'name': 'chat',
        'layout': 'monadtall',
        'position': 3,
        'exclusive': True,
        'persist': False,
        'init': False,
        'matches': [
            Match(wm_class=['discord', 'Signal'])
        ]

    },
    {
        'name': 'www',
        'layout': 'monadtall',
        'position': 4,
        'exclusive': True,
        'persist': False,
        'init': False,
        'matches': [
            Match(wm_class=['qutebrowser', 'Brave-browser'])
        ]
    },
    {
        'name': 'doc',
        'layout': 'monadtall',
        'position': 5,
        'exclusive': True,
        'persist': False,
        'init': False,
        'matches': [
            Match(wm_class=['libreoffice-calc'])
        ]
    }
]

groups = [Group(**kwargs) for kwargs in group_names]

dgroups_key_binder = simple_key_binder(mod)

groups += [
    ScratchPad("scratchpad", [
        DropDown("alacritty", "alacritty", opacity=1, width=0.8, height=0.8, x=0.1, y=0.07)
    ])
]

layout_theme = {
    'border_width': 3,
    'margin': 4,
    'border_focus': '88C0D0',
    'border_normal': '2E3440'
}

layouts = [
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Zoomy(**layout_theme),
    layout.Max(**layout_theme),
    layout.RatioTile(**layout_theme)
]

widget_defaults = dict(
    font='Anonymice Nerd Font',
    fontsize = 16,
    foreground = nord[6],
    background = nord[0],
    padding = 2
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
        widget.Sep(
            linewidth  = 0,
            foreground = nord[6],
            background = nord[0],
            padding    = 6
        ),
        widget.Image(
            filename        = '~/.config/qtile/icons/python-white.png',
            scale           = False,
            mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm)}
        ),
        widget.Sep(
            linewidth  = 0,
            foreground = nord[6],
            background = nord[0],
            padding    = 6
        ),
        widget.GroupBox(
            fontsize                    = 14,
            rounded                     = False,
            margin_y                    = 3,
            margin_x                    = 0,
            padding_y                   = 5,
            padding_x                   = 3,
            borderwidth                 = 3,
            active                      = nord[6],
            inactive                    = colors[7],
            highlight_color             = colors[1],
            highlight_method            = 'line',
            this_current_screen_border  = colors[6],
            this_screen_border          = colors[4],
            other_current_screen_border = colors[6],
            other_screen_border         = colors[4],
            foreground                  = nord[6],
            background                  = nord[0]
        ),
        widget.Sep(
            linewidth  = 0,
            foreground = nord[6],
            background = nord[0],
            padding    = 40
        ),
        widget.WindowName(
            foreground = colors[6],
            background = nord[0],
            padding    = 0
        ),
        widget.Systray(
            background = nord[0],
            padding    = 5
        ),
        widget.Sep(
            linewidth  = 0,
            foreground = nord[0],
            background = nord[0],
            padding    = 6
        ),
        widget.TextBox(
            font       = 'FontAwesome',
            fontsize   = 46,
            text       = '',
            foreground = colors[5],
            background = nord[0],
            padding    = 0
        ),
        widget.CPU(
            fmt        = '﬙ {}',
            threshold  = 90,
            foreground = nord[6],
            background = colors[5],
            padding    = 5
        ),
        widget.TextBox(
            font       = 'FontAwesome',
            fontsize   = 46,
            text       = '',
            foreground = colors[4],
            background = colors[5],
            padding    = 0
        ),
        widget.Memory(
            fmt             = ' {}',
            foreground      = nord[6],
            background      = colors[4],
            padding         = 5,
            mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
        ),
        widget.TextBox(
            font       = 'FontAwesome',
            fontsize   = 46,
            text       = '',
            foreground = colors[5],
            background = colors[4],
            padding    = 0
        ),
        widget.Net(
            interface  = 'enp37s0',
            format     = '↓ {down} ↑ {up}',
            foreground = nord[6],
            background = colors[5],
            padding    = 4
        ),
        widget.TextBox(
            font       = 'FontAwesome',
            fontsize   = 46,
            text       = '',
            foreground = colors[4],
            background = colors[5],
            padding    = 0,
        ),
        widget.TextBox(
            text       = 'ﯩ',
            foreground = nord[6],
            background = colors[4],
            padding    = 4
        ),
        widget.CheckUpdates(
            custom_command    = 'checkupdates+aur',
            update_interval   = 1800,
            display_format    = '{updates}',
            fmt               = '{} updates',
            no_update_string  = '0',
            colour_no_updates = nord[6],
            foreground        = nord[6],
            background        = colors[4],
            padding           = 6,
            mouse_callbacks   = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e yay')}
        ),
        widget.TextBox(
            font       = 'FontAwesome',
            fontsize   = 46,
            text       = '',
            foreground = colors[5],
            background = colors[4],
            padding    = 0
        ),
        widget.CurrentLayoutIcon(
            custom_icon_paths = [os.path.expanduser('~/.config/qtile/icons')],
            scale             = 0.7,
            foreground        = nord[0],
            background        = colors[5],
            padding           = 0
        ),
        widget.CurrentLayout(
            foreground = nord[6],
            background = colors[5],
            padding    = 5
        ),
        widget.TextBox(
            font       = 'FontAwesome',
            fontsize   = 46,
            text       = '',
            foreground = colors[4],
            background = colors[5],
            padding    = 0
        ),
        widget.Clock(
            format     = '%a, %b %d, %Y: %H:%M:%S ',
            foreground = nord[6],
            background = colors[4]
        ),
    ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1                   # Monitor 1 will display all widgets

def init_widgets_screen2():
	widgets_screen2 = init_widgets_list()
	del widgets_screen2[6:17]              # Slicing removes unwanted widgets on monitor 2
	return widgets_screen2

def init_screens():
    return [
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=0.9, size=24)),
        Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=0.9, size=24))
    ]

if __name__ in ['config', '__main__']:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

cursor_warp = False
auto_fullscreen = False
focus_on_window_activation = 'focus'
#wmname = 'LG3D'

dgroups_app_rules = []

floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
])

@hook.subscribe.startup_once
def autostart():
    subprocess.call([os.path.expanduser('~/.config/qtile/autostart.sh')])
