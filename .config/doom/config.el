(setq display-line-numbers-type 'relative
      auto-save-default t
      tab-width 2
      truncate-string-ellipsis "…")
(global-subword-mode t)

(setq user-full-name "Adam Zhang")

(setq doom-font (font-spec :family "Anonymice Nerd Font Mono" :size 18)
      doom-variable-pitch-font (font-spec :family "Anonymice Nerd Font" :size 18)
      doom-big-font (font-spec :family "Anonymice Nerd Font Mono" :size 28))

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

(setq doom-theme 'doom-tomorrow-night)

(define-advice evil-line-move (:around (orig-fun count &optional noerror))
  (let ((line-move-visual t))
    (apply orig-fun count noerror)))

(setq org-directory "~/BigBoiStorage/MEGA/Documents/Org/"
      org-agenda-files (directory-files-recursively org-directory "^[^.].*\\.org$" nil nil t)
      +org-capture-todo-file (concat org-directory "agenda.org")
      org-archive-location (concat org-directory "archive.org.archive::datetree/")
      org-log-done 'time
      org-hide-emphasis-markers t
      org-startup-folded 'content
      org-reverse-note-order t
      org-export-with-section-numbers nil
      org-export-with-toc nil
      org-ellipsis " ▼ "
      org-agenda-skip-scheduled-if-done t
      org-agenda-skip-deadline-if-done t
      org-agenda-todo-ignore-scheduled t)

(defun ad/org-export-output-file-name-modified (orig-fun extension &optional subtreep pub-dir)
  (unless pub-dir
    (setq pub-dir "exports/")
    (unless (file-directory-p pub-dir)
      (make-directory pub-dir)))
  (apply orig-fun extension subtreep pub-dir nil))
(advice-add 'org-export-output-file-name :around #'ad/org-export-output-file-name-modified)

(defun ad/org-export-get-final-location (file-name)
  (with-current-buffer (concat (file-name-sans-extension (file-name-nondirectory file-name)) ".org")
    (let ((file-extension (file-name-extension file-name)))
      (expand-file-name (concat "exports/" file-extension "/" (nth 1 (car (org-collect-keywords '("TITLE")))) "." file-extension)))))

(define-advice org-export-dispatch (:after (&rest lol-more-useless-than-phoenix))
  (let ((file-list (directory-files "exports" t directory-files-no-dot-files-regexp)))
    (while file-list
      (setq current-file (pop file-list)
            current-file-extension (file-name-extension current-file))
      (when current-file-extension
        (let ((final-path (ad/org-export-get-final-location current-file)))
          (let ((final-dir (file-name-directory final-path)))
            (unless (file-directory-p final-dir)
              (make-directory final-dir)))
          (when (file-exists-p final-path)
            (delete-file final-path))
          (rename-file current-file final-path))))))

(define-advice org-open-file (:around (orig-fun path &optional in-emacs line search))
  (when (string-match-p "exports" path)
    (setq path (ad/org-export-get-final-location path)))
  (apply orig-fun path in-emacs line search))

(setq org-roam-directory org-directory
      org-roam-capture-templates '(("d" "default" plain "%?"
                                    :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+TITLE: ${title}\n\n")
                                    :unnarrowed t)
                                   ("s" "school" plain "%?"
                                    :target (file+head "school/%<%Y%m%d%H%M%S>-${slug}.org" "#+TITLE: ${title}\n\n")
                                    :unnarrowed t))
      org-roam-mode-section-functions (list #'org-roam-backlinks-section
                                            #'org-roam-reflinks-section
                                            #'org-roam-unlinked-references-section)
      org-roam-graph-node-extra-config '(("id"
                                          ("style" . "bold,rounded,filled")
                                          ("fillcolor" . "#EEEEEE")
                                          ("color" . "#C9C9C9")
                                          ("fontcolor" . "#111111")
                                          ("shape" . "box"))
                                         ("http"
                                          ("style" . "rounded,filled")
                                          ("fillcolor" . "#EEEEEE")
                                          ("color" . "#C9C9C9")
                                          ("fontcolor" . "#0A97A6"))
                                         ("https"
                                          ("shape" . "rounded,filled")
                                          ("fillcolor" . "#EEEEEE")
                                          ("color" . "#C9C9C9")
                                          ("fontcolor" . "#0A97A6"))))
(org-roam-db-autosync-mode)

(setq org-roam-ui-sync-theme t
      org-roam-ui-follow t
      org-roam-ui-update-on-save t
      org-roam-ui-open-on-start nil)
(add-hook 'org-roam-mode-hook (lambda () (unless org-roam-ui-mode (org-roam-ui-mode))))

(setq school-directory (concat org-directory "school/")
      school-directory-html (concat school-directory "public/")
      org-publish-project-alist
      `(("school-org"
         :base-directory ,school-directory
         :base-extension "org"
         :recursive t
         :publishing-directory ,school-directory-html
         :publishing-function org-html-publish-to-html
         :auto-sitemap t
         :sitemap-title ""
         :sitemap-sort-files anti-chronologically)
        ("school-raw"
         :base-directory ,school-directory
         :base-extension "pdf"
         :recursive t
         :publishing-directory ,school-directory-html
         :publishing-function org-publish-attachment)
        ("school"
         :components ("school-org" "school-raw"))))

(map! :leader
      (:prefix "m"
      :desc "org-babel-tangle" "B" #'org-babel-tangle
      :desc "org-ctrl-c-ctrl-c" "C" #'org-ctrl-c-ctrl-c
      :desc "org-edit-src-code" "E" #'org-edit-src-code
      :desc "org-latex-preview" "L" #'org-latex-preview))

(after! org
  (setq org-todo-keywords '((sequence "TODO(t)" "PROJ(p)" "STARTED(s)" "WAITING(w)" "IDEA(i)" "|" "DONE(d)")
                            (sequence "[ ](c)" "[-](O)" "[?](W)" "|" "[X](D)")
                            (sequence "TEST(T)" "QUIZ(q)" "ASSIGNMENT(a)" "|" "DONE"))
        org-todo-keyword-faces '(("PROJ" . +org-todo-project)
                                 ("STARTED" . +org-todo-active)
                                 ("WAITING" . +org-todo-onhold)
                                 ("IDEA" . "#ECEFF4")
                                 ("TEST" . +org-todo-cancel)
                                 ("QUIZ" . "#EBCB8B")
                                 ("ASSIGNMENT" . "#D08770"))))

(setq org-capture-templates '(("t" "Personal todo" entry
  (file+headline +org-capture-todo-file "Inbox")
  "* [ ] %?\n%i\n%a" :prepend t)
 ("n" "Personal notes" entry
  (file+headline +org-capture-notes-file "Inbox")
  "* %u %?\n%i\n%a" :prepend t)
 ("j" "Journal" entry
  (file+olp+datetree +org-capture-journal-file)
  "* %U %?\n%i\n%a" :prepend t)
 ("p" "Templates for projects")
 ("pt" "Project-local todo" entry
  (file+headline +org-capture-project-todo-file "Inbox")
  "* TODO %?\n%i\n%a" :prepend t)
 ("pn" "Project-local notes" entry
  (file+headline +org-capture-project-notes-file "Inbox")
  "* %U %?\n%i\n%a" :prepend t)
 ("pc" "Project-local changelog" entry
  (file+headline +org-capture-project-changelog-file "Unreleased")
  "* %U %?\n%i\n%a" :prepend t)
 ("o" "Centralized templates for projects")
 ("ot" "Project todo" entry #'+org-capture-central-project-todo-file "* TODO %?\n %i\n %a" :heading "Tasks" :prepend nil)
 ("on" "Project notes" entry #'+org-capture-central-project-notes-file "* %U %?\n %i\n %a" :heading "Notes" :prepend t)
 ("oc" "Project changelog" entry #'+org-capture-central-project-changelog-file "* %U %?\n %i\n %a" :heading "Changelog" :prepend t)))

(map! :leader
      (:prefix "l"
      :desc "ispell" "s" #'ispell
      :desc "langtool-check" "c" #'langtool-check
      :desc "langtool-correct-buffer" "l" #'langtool-correct-buffer
      :desc "langtool-check-done" "d" #'langtool-check-done))
