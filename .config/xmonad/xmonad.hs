-- IMPORTS
import XMonad
import XMonad.Util.EZConfig       (additionalKeysP)
import XMonad.Hooks.EwmhDesktops  (ewmh)
import XMonad.Hooks.StatusBar     (withSB, statusBarProp)
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.ManageDocks   (avoidStruts, docks)
import XMonad.Hooks.FadeInactive  (fadeInactiveLogHook)
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders    (noBorders)
import XMonad.Hooks.ManageHelpers (doCenterFloat)
import XMonad.StackSet            (swapMaster)
import XMonad.Util.Hacks          (windowedFullscreenFixEventHook)

-- Nord color theme declarations for clarity
-- <3 Nord theme
nord0  = "#2E3440"
nord1  = "#3B4252"
nord2  = "#434C5E"
nord3  = "#4C566A"
nord4  = "#D8DEE9"
nord5  = "#E5E9F0"
nord6  = "#ECEFF4"
nord7  = "#8FBCBB"
nord8  = "#88C0D0"
nord9  = "#81A1C1"
nord10 = "#5E81AC"
nord11 = "#BF616A"
nord12 = "#D08770"
nord13 = "#EBCB8B"
nord14 = "#A3BE8C"
nord15 = "#B48EAD"

-- Call to main method
-- XMonad defaults -> true fullscreen -> ewmh compliance (helps with random problems) -> xmobar -> myConfig
main = xmonad 
    -- $ ewmhFullscreen
    $ ewmh 
    $ withSB (statusBarProp "xmobar -x 0 ~/.config/xmobar/xmobarrc0" (pure myXmobarPP))
    $ withSB (statusBarProp "xmobar -x 1 ~/.config/xmobar/xmobarrc1" (pure myXmobarPP))
    $ docks
    $ myConfig

-- Actual config in this function
myConfig = def
  {
    terminal           = "alacritty",
    modMask            = mod4Mask,      -- super/windows key
    layoutHook         = myLayout,      -- use my custom layout
    borderWidth        = 2,             -- border thickness
    normalBorderColor  = nord0,         -- border colors
    focusedBorderColor = nord8,
    --logHook            = myLogHook,     -- use my log hook
    manageHook         = myManageHook,  -- use my manage hook
    workspaces         = myWorkspaces,
    handleEventHook    = handleEventHook def <+> windowedFullscreenFixEventHook
  }
  -- Custom keybindings
  `additionalKeysP`
    [
      -- Emacs-style keybindings, M = super/mod
      -- Much better shortcuts for application launching
      ("M-<Return>",  spawn "rofi -show drun"),   -- Rofi application launcher
      ("M-S-m",       windows swapMaster),        -- M-<Return> is swap master by default so add another keybinding
      -- M-p set to launch misc searches
      ("M-p p",       spawn "rofi -show drun"),
      ("M-p r",       spawn "rofi -show run"),
      ("M-p <Space>", spawn "rofi -show window"),
      ("M-p f",       spawn "rofi -show filebrowser"),

      -- Lock screen
      ("M-<Esc>", spawn ".local/bin/lock"),
      -- Open programs
      ("M-b",     spawn "qutebrowser"),
      ("M-S-b",   spawn "torbrowser-launcher"),
      ("<Print>", spawn "flameshot gui"),
      ("M-v",     spawn "emacsclient -c"),

      -- mpv controls
      ("<XF86AudioPlay>", spawn "xdotool key --window \"$(xdotool search --classname alacritty-mpv)\" space"),
      ("<XF86AudioPrev>", spawn "xdotool key --window \"$(xdotool search --classname alacritty-mpv)\" less"),
      ("<XF86AudioNext>", spawn "xdotool key --window \"$(xdotool search --classname alacritty-mpv)\" greater")
    ]

-- My xmobar formatting/config (pretty-printer)
myXmobarPP = def
  {
    ppCurrent         = xmobarColor nord8 "" . wrap ("<box type=Bottom width=2 mb=2 color="++nord13++">") "</box>", -- Current workspace
    ppVisible         = xmobarColor nord7 "" . wrap ("<box type=Bottom width=2 mb=2 color="++nord14++">") "</box>", -- Visible but not current workspace
    ppHidden          = xmobarColor nord9 "" . wrap ("<box type=Top width=2 mt=2 color="++nord15++">") "</box>",    -- Hidden workspaces
    ppHiddenNoWindows = xmobarColor nord5 "",                          -- Hidden workspaces (no windows)
    ppTitle           = xmobarColor nord6 "" . shorten 60,             -- Title of active window
    ppSep             =  "<fc="++nord3++"> | </fc>",                   -- Separator character
    ppUrgent          = xmobarColor nord11 "" . wrap "!" "!",          -- Urgent workspace
    ppOrder           = \(ws:l:t:ex) -> [ws,l]++ex++[t]                -- order of things in xmobar
  }

-- My layout setup
-- Reorders Tall and Full for easy switching
-- As well as makes sure xmobar stays visible
-- spacingRaw adds gaps, True = smart gaps, screen borders (off), window borders
-- Avoid the panel on tiled layouts, but fullscreen should be fullscreen (no panel or borders)
myLayout = spacingRaw True (Border 0 0 0 0) False (Border 4 4 4 4) True $
  avoidStruts tiled ||| noBorders Full ||| avoidStruts (Mirror tiled)
    where
      tiled    = Tall nmaster delta ratio
      nmaster  = 1      -- Default number of windows in the master pane
      ratio    = 1/2    -- Default proportion of screen occupied by master pane
      delta    = 3/100  -- Percent of screen to increment by when resizing panes

-- My log hook
-- Don't really know what this is but it lets me make inactive windows
-- transparent and I ripped this all from the docs so pog?
--myLogHook = fadeInactiveLogHook 0.9

-- My workspaces
-- Custom names are hot <3
myWorkspaces = ["dev", "chat", "info", "www", "doc", "6", "7", "8", "9"]

-- My manage hook
-- Automagically applies rules to windows that match certain properties
-- (like move to certain tag, float, etc)
myManageHook = composeAll
  [
    className =? "@joplinapp-desktop" --> doCenterFloat, -- float Joplin's confirmation dialogs to the center

    -- doShift moves to a certain tag
    -- workspaces !! n moves to the nth workspace
    -- so I don't have to type out names
    className =? "discord"          --> doShift (myWorkspaces !! 1),
    className =? "Joplin"           --> doShift (myWorkspaces !! 2),
    appName   =? "alacritty-joplin" --> doShift (myWorkspaces !! 2),
    appName   =? "alacritty-mpv"    --> doShift (myWorkspaces !! 2)
  ]
