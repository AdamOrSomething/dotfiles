config.load_autoconfig(True)

c.aliases = {'q': 'quit', 'w': 'session-save', 'wq': 'quit --save', 'bw': 'spawn --userscript qute-bitwarden', 'bwpw': 'spawn --userscript qute-bitwarden --password-only'}

c.content.blocking.method = 'both'

config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')

c.content.canvas_reading = False

c.content.webgl = False
config.set('content.webgl', True, 'http://localhost:35901')

c.content.headers.custom = {'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}

c.content.headers.accept_language = 'en-US,en;q=0.5'

c.content.headers.user_agent = 'Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0'
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:90.0) Gecko/20100101 Firefox/90.0', 'https://accounts.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {qt_key}/{qt_version} {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://*.gitlab.com/*')

c.content.javascript.enabled = True
config.set('content.javascript.enabled', True, 'chrome-devtools://*')
config.set('content.javascript.enabled', True, 'devtools://*')
config.set('content.javascript.enabled', True, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')

c.colors.webpage.darkmode.enabled = False

c.content.autoplay = False

c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}', 'aw': 'https://wiki.archlinux.org/?search={}', 're': 'https://www.reddit.com/r/{}', 'wiki': 'https://en.wikipedia.org/wiki/{}', 'yt': 'https://www.youtube.com/results?search_query={}'}

config.bind(',m', 'spawn mpv {url}')
config.bind(';M', 'hint links spawn mpv {hint-url}')

config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')

c.spellcheck.languages = ['en-US']

config.source('nord-qutebrowser.py')
