#+TITLE: Qutebrowser Config
#+PROPERTY: header-args :tangle config.py

* Table of Contents :toc:
- [[#about][About]]
- [[#load-autoconfig][Load Autoconfig]]
- [[#aliases][Aliases]]
- [[#adblock][Adblock]]
- [[#cookies][Cookies]]
- [[#canvaswebgl][Canvas/WebGL]]
  - [[#canvas][Canvas]]
  - [[#webgl][WebGL]]
- [[#headers][Headers]]
  - [[#accept][Accept]]
  - [[#language][Language]]
  - [[#user-agent][User Agent]]
- [[#javascript][JavaScript]]
- [[#dark-theme][Dark Theme]]
- [[#autoplay][Autoplay]]
- [[#search-engines][Search Engines]]
- [[#custom-keybindings][Custom Keybindings]]
  - [[#play-youtube-videos-with-mpv][Play YouTube Videos with Mpv]]
  - [[#toggle-ui-elements][Toggle UI elements]]
- [[#spellcheck][Spellcheck]]
- [[#theme][Theme]]

* About
NOTE: config.py is intended for advanced users who are comfortable with manually migrating the config file on qutebrowser upgrades. If you prefer, you can also configure qutebrowser using the :set/:bind/:config-* commands without having to write a config.py file.

Documentation:
+ qute://help/configuring.html
+ qute://help/settings.html

* Load Autoconfig
Load ~autoconfig.yml~ for permissions (notifications, etc)
No configuration should be in ~autoconfig.yml~, it should only to persist permissions.

#+BEGIN_SRC python
config.load_autoconfig(True)
#+END_SRC

* Aliases
Aliases for commands.
| Alias | Description                                         |
|-------+-----------------------------------------------------|
| q     | Quit                                                |
| w     | Save session                                        |
| wq    | Save and quit                                       |
| bw    | Launch Bitwarden userscript                         |
| bwpw  | Launch Bitwarden userscript and only input password |

#+BEGIN_SRC python
c.aliases = {'q': 'quit', 'w': 'session-save', 'wq': 'quit --save', 'bw': 'spawn --userscript qute-bitwarden', 'bwpw': 'spawn --userscript qute-bitwarden --password-only'}
#+END_SRC

* Adblock
Which method of blocking ads should be used. Support for Adblock Plus (ABP) syntax blocklists using Brave's Rust library requires the ~adblock~ Python package to be installed, which is an optional dependency of qutebrowser. It is required when either ~adblock~ or ~both~ are selected.
Type: ~String~
Valid values:
  + ~auto~: Use Brave's ABP-style adblocker if available, host blocking otherwise
  + ~adblock~: Use Brave's ABP-style adblocker
  + ~hosts~: Use hosts blocking
  + ~both~: Use both hosts blocking and Brave's ABP-style adblocker

#+BEGIN_SRC python
c.content.blocking.method = 'both'
#+END_SRC

* Cookies
Which cookies to accept. With QtWebEngine, this setting also controls other features with tracking capabilities similar to those of cookies; including IndexedDB, DOM storage, filesystem API, service workers, and AppCache. Note that with QtWebKit, only `all` and `never` are supported as per-domain values. Setting `no-3rdparty` or `no- unknown-3rdparty` per-domain on QtWebKit will have the same effect as `all`. If this setting is used with URL patterns, the pattern gets applied to the origin/first party URL of the page making the request, not the request URL. With QtWebEngine 5.15.0+, paths will be stripped from URLs, so URL patterns using paths will not match. With QtWebEngine 5.15.2+, subdomains are additionally stripped as well, so you will typically need to set this setting for `example.com` when the cookie is set on `somesubdomain.example.com` for it to work properly. To debug issues with this setting, start qutebrowser with `--debug --logfilter network --debug-flag log-cookies` which will show all cookies being set.
Type: ~String~
Valid values:
  + ~all~: Accept all cookies.
  + ~no-3rdparty~: Accept cookies from the same origin only. This is known to break some sites, such as Gmail.
  + ~no-unknown-3rdparty~: Accept cookies from the same origin only, unless a cookie is already set for the domain. On QtWebEngine, this is the same as no-3rdparty.
  + ~never~: Don't accept cookies at all.

#+BEGIN_SRC python
config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')
#+END_SRC

* Canvas/WebGL
** Canvas
Allow websites to read canvas elements. Note this is needed for some websites to work properly.
Type: ~Bool~

#+BEGIN_SRC python
c.content.canvas_reading = False
#+END_SRC

** WebGL
Enable WebGL.
Type: ~Bool~

#+BEGIN_SRC python
c.content.webgl = False
config.set('content.webgl', True, 'http://localhost:35901')
#+END_SRC

* Headers
Custom headers for qutebrowser HTTP requests.

** Accept

#+BEGIN_SRC python
c.content.headers.custom = {'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
#+END_SRC

** Language
Value to send in the ~Accept-Language~ header. Note that the value read from JavaScript is always the global value.
Type: ~String~

#+BEGIN_SRC python
c.content.headers.accept_language = 'en-US,en;q=0.5'
#+END_SRC

** User Agent
User agent to send.  The following placeholders are defined:
+ ~{os_info}~: Something like "X11; Linux x86_64".
+ ~{webkit_version}~: The underlying WebKit version (set to a fixed value   with QtWebEngine).
+ ~{qt_key}~: "Qt" for QtWebKit, "QtWebEngine" for QtWebEngine.
+ ~{qt_version}~: The underlying Qt version.
+ ~{upstream_browser_key}~: "Version" for QtWebKit, "Chrome" for QtWebEngine.
+ ~{upstream_browser_version}~: The corresponding Safari/Chrome version.
+ ~{qutebrowser_version}~: The currently running qutebrowser version.  The default value is equal to the unchanged user agent of QtWebKit/QtWebEngine.  Note that the value read from JavaScript is always the global value. With QtWebEngine between 5.12 and 5.14 (inclusive), changing the value exposed to JavaScript requires a restart.
Type: ~FormatString~

#+BEGIN_SRC python
c.content.headers.user_agent = 'Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0'
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:90.0) Gecko/20100101 Firefox/90.0', 'https://accounts.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {qt_key}/{qt_version} {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://*.gitlab.com/*')
#+END_SRC

* JavaScript
Enable JavaScript.
Type: ~Bool~

#+BEGIN_SRC python
c.content.javascript.enabled = True
config.set('content.javascript.enabled', True, 'chrome-devtools://*')
config.set('content.javascript.enabled', True, 'devtools://*')
config.set('content.javascript.enabled', True, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')
#+END_SRC

* Dark Theme
Render all web contents using a dark theme. Example configurations from Chromium's ~chrome://flags~: 
  + "With simple HSL/CIELAB/RGB-based inversion": Set   ~colors.webpage.darkmode.algorithm~ accordingly. 
  + "With selective image inversion": Set ~colors.webpage.darkmode.policy.images~ to ~smart~. 
  + "With selective inversion of non-image elements": Set ~colors.webpage.darkmode.threshold.text~ to 150 and ~colors.webpage.darkmode.threshold.background~ to 205. 
  + "With selective inversion of everything": Combines the two variants above.
Type: ~Bool~

#+BEGIN_SRC python
c.colors.webpage.darkmode.enabled = False
#+END_SRC

* Autoplay
Turn off video autoplay.

#+BEGIN_SRC python
c.content.autoplay = False
#+END_SRC

* Search Engines
Defines available search engines along with aliases.
+ ~DEFAULT~: DuckDuckGo
+ ~aw~: ArchWiki
+ ~re~: Reddit
+ ~wiki~: Wikipedia
+ ~yt~: YouTube

#+BEGIN_SRC python
c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}', 'aw': 'https://wiki.archlinux.org/?search={}', 're': 'https://www.reddit.com/r/{}', 'wiki': 'https://en.wikipedia.org/wiki/{}', 'yt': 'https://www.youtube.com/results?search_query={}'}
#+END_SRC

* Custom Keybindings
** Play YouTube Videos with Mpv
| Keybinding | Description               |
|------------+---------------------------|
| ,m         | Open mpv with current URL |
| ;M         | Open mpv with hinted URL  |

#+BEGIN_SRC python
config.bind(',m', 'spawn mpv {url}')
config.bind(';M', 'hint links spawn mpv {hint-url}')
#+END_SRC

** Toggle UI elements
| Keybinding | Description                        |
|------------+------------------------------------|
| xb         | Toggle statusbar                   |
| xt         | Toggle tabs                        |
| xx         | Toggle both the statusbar and tabs |

#+BEGIN_SRC python
config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')
#+END_SRC

* Spellcheck
Enable spellcheck for English.

#+BEGIN_SRC python
c.spellcheck.languages = ['en-US']
#+END_SRC

* Theme
Source the Nord theme file.

#+BEGIN_SRC python
config.source('nord-qutebrowser.py')
#+END_SRC
