if status is-login
	if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
		exec startx -- -keeptty
	end
end

set fish_greeting

if test -d ~/.local/bin
  if not contains -- ~/.local/bin $PATH
    set -p PATH ~/.local/bin
  end
end
# ~/.emacs.d/bin (DOOM Emacs)
if test -d ~/.emacs.d/bin
  if not contains -- ~/.emacs.d/bin $PATH
    set -p PATH ~/.emacs.d/bin
  end
end

if status is-interactive
    colorscript --random

    alias la='exa -al --color=always --group-directories-first --icons' # preferred listing
    alias ls='exa -a --color=always --group-directories-first --icons'  # all files and dirs
    alias ll='exa -l --color=always --group-directories-first --icons'  # long format
    alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
    alias l.="exa -a | egrep '^\.'"                                     # show only dotfiles

    alias bw='bw --session=$BW_SESSION'

    alias config='git --git-dir=$HOME/.cfg --work-tree=$HOME'

    abbr --add jrnl " jrnl"

    starship init fish | source

    thefuck --alias | source
end
